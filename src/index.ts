import { foo } from './example';

function add(arg: number) {
  return arg + 1;
}

console.log(foo('Hello remote'), add(2));
